﻿using AdventureWorksProduct.DbModel;
using AdventureWorksProduct.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Controllers
{
    /// <summary>
    /// Processed requests to Product resource.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductApiController : ControllerBase
    {
        private readonly AdventureWorks2019Context _context;

        /// <summary>
        /// Initializes a new instance of <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ProductApiController(AdventureWorks2019Context context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all products.
        /// </summary>
        /// <returns>Return all products.</returns>
        // GET: api/ProductApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProduct()
        {
            return await _context.Product.ToListAsync();
        }

        /// <summary>
        /// Gets the product by ProductId.
        /// </summary>
        /// <param name="id">Id of product.</param>
        /// <returns>The product.</returns>
        // GET: api/ProductsApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            Log.Logger.Information($" Gets the product by ProductId = {id}.");
            var product = await _context.Product.FindAsync(id);

            if (product == null)
            {
                throw new EntityNotFoundException();
            }

            return product;
        }

        /// <summary>
        /// Update product.
        /// </summary>
        /// <param name="id">Id of product.</param>
        /// <param name="product">Product.</param>
        /// <returns>No content.</returns>
        // PUT: api/ProductApi/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.ProductId)
            {
                throw new ValidationException();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    throw new EntityNotFoundException();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add new product.
        /// </summary>
        /// <param name="product">New product.</param>
        /// <returns></returns>
        // POST: api/ProductApi
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.ProductId }, product);
        }

        /// <summary>
        /// Delete product.
        /// </summary>
        /// <param name="id">Product id.</param>
        /// <returns>No content.</returns>
        // DELETE: api/ProductApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                throw new EntityNotFoundException();
            }

            var transactionHistory = _context.TransactionHistory.Where(i => i.ProductId == id);
            _context.TransactionHistory.RemoveRange(transactionHistory);

            var billOfMaterials = _context.BillOfMaterials.Where(i => i.ComponentId == id);
            _context.BillOfMaterials.RemoveRange(billOfMaterials);

            var productInventory = _context.ProductInventory.Where(i => i.ProductId == id);
            _context.ProductInventory.RemoveRange(productInventory);

            var productProductPhoto = _context.ProductProductPhoto.Where(i => i.ProductId == id);
            _context.ProductProductPhoto.RemoveRange(productProductPhoto);

            var productVendor = _context.ProductVendor.Where(i => i.ProductId == id);
            _context.ProductVendor.RemoveRange(productVendor);

            var purchaseOrderDetail = _context.PurchaseOrderDetail.Where(i => i.ProductId == id);
            _context.PurchaseOrderDetail.RemoveRange(purchaseOrderDetail);

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.ProductId == id);
        }
    }
}
