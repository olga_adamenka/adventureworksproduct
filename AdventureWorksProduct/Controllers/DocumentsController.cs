﻿using AdventureWorksProduct.Models;
using AdventureWorksProduct.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private IFileBlobService fileBlobService;

        public DocumentsController(IFileBlobService fileBlobService)
        {
            this.fileBlobService = fileBlobService;
        }

        [HttpPost]
        public async Task<IActionResult> PostDocument(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest();
            }

            try
            {
                using (var stream = file.OpenReadStream())
                {
                    var documentModel = new DocumentModel(file.FileName);
                    var azureFileName = await fileBlobService.UploadFileAsync(documentModel, stream);
                }
            }
            catch(Exception ex)
            {
                return Problem(detail: ex.Message);
            }

            return Ok();
        }
    }
}
