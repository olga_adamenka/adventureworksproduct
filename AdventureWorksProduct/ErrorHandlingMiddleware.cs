﻿using Microsoft.AspNetCore.Http;
using System;
using Serilog;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using AdventureWorksProduct.Exceptions;
using Serilog.Events;

namespace AdventureWorksProduct
{
    /// <summary>
    /// The middleware to handle application exceptions and convert them into corresponding status codes.
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Processes application exceptions and converts them into HTTP status codes.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="ex">The exception to process.</param>
        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError;

            // Process application logic exception.
            if (ex is EntityNotFoundException)
            {
                code = HttpStatusCode.NotFound;
                Log.Logger.Error(ex, $" Processing request exception - EntityNotFoundException.  HttpStatusCode = {code}.");
            }
            else if (ex is ValidationException)
            {
                code = HttpStatusCode.BadRequest;
                Log.Logger.Error(ex, $" Processing request exception - ValidationException.  HttpStatusCode = {code}.");
            }

            if (code == HttpStatusCode.InternalServerError)
            {
                Log.Logger.Error(ex, $" Processing request exception - InternalServerError.  HttpStatusCode = {code}.");
            }

            var result = JsonConvert.SerializeObject(new { error = ex.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
