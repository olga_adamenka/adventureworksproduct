using AdventureWorksProduct.Configurations;
using AdventureWorksProduct.DbModel;
using AdventureWorksProduct.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using Azure.Core;

namespace AdventureWorksProduct
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.Configure<AzureSettingsOptions>(Configuration.GetSection(AzureSettingsOptions.AzureSettings));
            
            services.AddScoped<AdventureWorks2019Context>();
            services.AddScoped<IFileBlobService, FileBlobService>();
            services.AddScoped<IQueueService, QueueService>();

            services.AddControllers(options =>
            {
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();

            });

            services.AddSwaggerGen( c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "AdventureWorksProduct API",
                    Description = "AdventureWorksProduct description"
                });
                c.IncludeXmlComments(GetXmlCommentsPath());
            }
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<AzureSettingsOptions> options)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            // Key vault
            SecretClientOptions secretOptions = new SecretClientOptions()
            {
                Retry =
                {
                    Delay= TimeSpan.FromSeconds(2),
                    MaxDelay = TimeSpan.FromSeconds(16),
                    MaxRetries = 5,
                    Mode = RetryMode.Exponential
                }
            };
            var client = new SecretClient(new Uri("https://webapp-products-keyvault.vault.azure.net/"), new DefaultAzureCredential(), secretOptions);
            string storageConnection = client.GetSecret("StorageConnectionString").Value.Value;
            string storageTableName = client.GetSecret("StorageTableName").Value.Value;

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            ConfigureLogger(storageConnection, storageTableName);
        }

        private static string GetXmlCommentsPath()
        {
            string path = $@"{AppDomain.CurrentDomain.BaseDirectory}\AdventureWorksProduct.xml";
            return path;
        }

        private static void ConfigureLogger(string storageConnection, string storageTableName) 
        {
            var storage = CloudStorageAccount.Parse(storageConnection);

            Log.Logger = new LoggerConfiguration()
                .WriteTo.File("logs\\log-.txt", rollingInterval: RollingInterval.Day)
                .WriteTo.AzureTableStorage(storage, storageTableName: storageTableName)
                .CreateLogger();
        }
    }
}
