﻿namespace AdventureWorksProduct.Configurations
{
    public class AzureSettingsOptions
    {
        public const string AzureSettings = "AzureSettings";

        public string QueueName { get; set; }
        public string ContainerName { get; set; }
        public string StorageTableName { get; set; }
        public string StorageConnectionString { get; set; }
        public string AccountName { get; set; }
        public string KeyValue { get; set; }
        public string BaseUrl { get; set; }
    }
}
