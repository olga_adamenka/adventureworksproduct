﻿using AdventureWorksProduct.Configurations;
using AdventureWorksProduct.Models;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Services
{
    internal class FileBlobService : IFileBlobService
    {
        private readonly AzureSettingsOptions options;
        private CloudBlobClient cloudBlobClient;

        private IQueueService queueService;

        public FileBlobService(IQueueService queueService, IOptions<AzureSettingsOptions> options)
        {
            this.queueService = queueService;
            this.options = options.Value;
            var credentials = new StorageCredentials(this.options.AccountName, this.options.KeyValue);
            cloudBlobClient = new CloudBlobClient(new Uri(this.options.BaseUrl), credentials);
        }

        public async Task<string> UploadFileAsync(DocumentModel document, Stream stream)
        {
            // upload document
            var container = cloudBlobClient.GetContainerReference(options.ContainerName);
            var blob = container.GetBlockBlobReference(document.BlobName);

            await blob.UploadFromStreamAsync(stream);

            // send notification
            await SendMessageAsync(document);

            return document.BlobName;
        }

        private Task SendMessageAsync(DocumentModel document)
        {
            var message = JsonSerializer.Serialize(document);
            return queueService.SendMessageToQueueAsync(message);
        }
    }
}
