﻿using AdventureWorksProduct.Configurations;
using Azure.Storage.Queues;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Services
{
    public class QueueService : IQueueService
    {
        QueueClient queueClient;
        private readonly AzureSettingsOptions options;

        public QueueService(IOptions<AzureSettingsOptions> options)
        {
            this.options = options.Value;
            queueClient = new QueueClient(
                this.options.StorageConnectionString,
                this.options.QueueName,
                new QueueClientOptions
                {
                    MessageEncoding = QueueMessageEncoding.Base64
                });
        }

        public Task SendMessageToQueueAsync(string message)
        {
             return queueClient.SendMessageAsync(message);
        }
    }
}
