﻿using AdventureWorksProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Services
{
    public interface IQueueService
    {
        Task SendMessageToQueueAsync(string message);
    }
}
