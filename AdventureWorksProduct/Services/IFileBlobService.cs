﻿using AdventureWorksProduct.Models;
using System.IO;
using System.Threading.Tasks;

namespace AdventureWorksProduct.Services
{
    public interface IFileBlobService
    {
        Task<string> UploadFileAsync(DocumentModel document, Stream stream);
    }
}
