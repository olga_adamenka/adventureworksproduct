﻿using System;
using System.Collections.Generic;

namespace AdventureWorksProduct.Models
{
    public class DocumentModel
    {
        public string FileName { get; private set; }

        public string BlobName { get; private set; }

        public Dictionary<string, string> Metadata { get; private set; }

        public DocumentModel(string fileName)
        {
            FileName = fileName;
            BlobName = Guid.NewGuid().ToString();
            Metadata = new Dictionary<string, string>();
            Metadata.Add("FileName", fileName);
        }
    }
}
